using System.Collections.Generic;

namespace EventManagmentFin.Models
{
    public class Speaker
    {
        public string ID {get;set;}
        public string Name {get;set;}
        public string Title{get;set;}
        public string Contact {get;set;}
        public string Company{get;set;}

        public string Email{get;set;}

                public ICollection<SpeakerAdded> SpeakerAdded {get;set;}

        
    }
}