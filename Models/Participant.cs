using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EventManagmentFin.Models {
    public class Participant {
        
        public string ID { get; set; } //system generated
        [Required (ErrorMessage = "Please Enter Your Name")]
        public String Name { get; set; } // user entered
        public DateTime Date { get; set; } // system generated
        public virtual ICollection<Registration> Registrations { get; set; } // registered  0----->n events

        

    }
}
