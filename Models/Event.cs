using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EventManagmentFin.Models {
    public class Event {
        [Key]
        public String ID { get; set; } //self generated

        [Required (ErrorMessage = "Event type Required")]
        //[Required]

        public string Type { get; set; } //user added

        [Required (ErrorMessage = "Event Name Required")]
        // [Required]
        public string Name { get; set; } //user added

        public string ImageUrl { get; set; } // add a url for your image

        [Required (ErrorMessage = "Please add an Expected/exact Event Date")]
        //  [Required]
        public DateTime Date { get; set; } //user added

        public string Status { get; set; } // auto filled

        public string EventDescription { get; set; }

        public ICollection<SpeakerAdded> SpeakerAdded {get;set;}
        

        //What is this event About
        // public virtual ICollection<Participant> Participant { get; set; } // Events n...>n participants // get a registered tables here 
        //break it using registered table....  DONE 

    }
}