namespace EventManagmentFin.Models {
    public class Department : Member {

        public string ID { get; set; }
        public string Name { get; set; }

        public string Head_ID { get; set; }

        public string CHead_ID { get; set; }

    }
}