using System.Collections.Generic;

namespace EventManagmentFin.Models
{
    public class Registration
    {
        public string ID {get;set;}
        public virtual ICollection<Registered> Registereds { get; set; } // registered  0----->n events

    }
}